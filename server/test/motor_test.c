#include<stdio.h>

#include "../include/myServo.h"

int main(){
	int cmd;
	while(1) {
		printf("0: close, 1: open, -1: exit\n");
		printf("cmd: ");
		scanf("%d", &cmd);
		if(cmd==-1) break;
		if(cmd!=0 && cmd !=1) {
			printf("invalid command!\n");
			continue;
		}
		door_lock(cmd);
	}

	return 0;
}

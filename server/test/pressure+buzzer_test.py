#!/usr/bin/python
import RPi.GPIO as GPIO
import time
 
#GPIO SETUP
channel = 21 #reset of the pin
buzzer = 23 #reset of the pin
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)  #The PIN / PORT number of GPIO is used as BCM mode.
GPIO.setup(channel, GPIO.IN) #Input / output direction can be set through GPIO.setup function.
GPIO.setup(buzzer, GPIO.OUT)

#Custom alert Callback function that every time it detects pressure or the state changes corresponding output
def callback(channel):
        if GPIO.input(channel): #signal which implies this sensor detect pressure when the light turns on
                with open('log.txt','a') as f:
                        f.write("Pushed")
                        f.write('\n')
                GPIO.output(buzzer,GPIO.HIGH)
                print ("Push")
        else: #signal which implies this sensor does not detect pressure when the light turns off
                with open('log.txt','a') as f:
                        f.write("UnPushed")
                        f.write('\n')
                GPIO.output(buzzer,GPIO.LOW)
                print ("Unpushed")
 
#functions that when the state changes detected, its function called
GPIO.add_event_detect(channel, GPIO.BOTH, bouncetime=300)  # let us know when the pin goes HIGH or LOW
GPIO.add_event_callback(channel, callback)  # assign function to GPIO PIN, Run function on change
 
# infinite loop
while True:
        time.sleep(1) #To stop the program and wait for one second
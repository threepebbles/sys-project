#include<stdio.h>
#include<unistd.h>

#include "../include/myBuzzer.h"

int main(){
	buzzer_off();
	while(1){
		int a;
		scanf("%d", &a);
		if(a == 0){
			buzzer_off();
		}
		else{
			buzzer_on();
		}
	}

	return 0;
}

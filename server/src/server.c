#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>

#include "../include/myBtn.h"

#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>

#define PORT 6789
#define MAX_BUF_SIZE 1024
const char close_msg[] = "close";
const char quit_msg[] = "quit";

char buf[MAX_BUF_SIZE];
	
void door_lock_loop();
void* listen_loop(void* arg);

int main() {
	pthread_t listen_thread;
	int err;
	err = pthread_create(&listen_thread, NULL, (void*)listen_loop, (void *)0);
	if(err!=0) {
		perror("pthread create failed\n");
		exit(-1);
	}
	door_lock_loop();
	return 0;
}

void door_lock_loop() {
	door_lock(DOOR_CLOSE);
	program_init();
	program_on();
}

void* listen_loop(void* arg) {
	int server_socket, client_socket;
	struct sockaddr_in server_addr, client_addr;
	int client_addr_size;

	server_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (server_socket == -1){
	    perror("socket() failed\n");
	    exit(-1);
	}

	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(PORT);
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind(server_socket, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1) {
	    perror("bind() failed\n");
	    exit(-1);
	}

	if(listen(server_socket, 1) == -1) {
	    perror("listen() failed\n");
	    exit(-1);
	}

	while(1) {
		printf("[SERVER] Waiting for client...\n");
		client_addr_size = sizeof(client_socket);    // connection socket
		client_socket = accept(server_socket, (struct sockaddr *)&client_addr, &client_addr_size); //wait for call
		if(client_socket == -1){
		    printf("accept() failed\n");
		    sleep(2); continue;
		}
		printf("[SERVER] Connected!\n");
		while(1) {
			memset(buf, 0, sizeof(buf));
			recv(client_socket, buf, sizeof(buf), 0); // receive message from client(blocking read)
			sleep(1);
			if(strcmp(buf, close_msg)==0) {
				door_lock(DOOR_CLOSE);
				pthread_mutex_lock(&mutex);
				set_lock();
				pthread_mutex_unlock(&mutex);
				printf("[SERVER] Door is closed\n");
			}
			else if(strcmp(buf, quit_msg)==0) {
				printf("[SERVER] Disconnected!\n");
				break;
			}
			else {
				printf("[SERVER] Invalid message from client: %s\n", buf);
			}
		}
	}
}
